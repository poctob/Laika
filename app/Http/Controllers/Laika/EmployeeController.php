<?php

namespace App\Http\Controllers\Laika;

use Illuminate\Http\Request;

class EmployeeController extends DBObjectController {

    protected function getObjectName() {
        return 'Laika\Employee';
    }

    protected function saveJSON(Request $request, $object) {
        $active = Input::get('active');
        $object->active = isset($active) ? $active : 0;
        $object->address = Input::get('address');
        $object->comment = Input::get('comment');
        $object->email = Input::get('email');
        $object->name = Input::get('name');
        $object->phone = Input::get('phone');

        $object->save();
    }

    protected function save(Request $request) {
        $this->validateEmployee($request);
        
        $employee = new \Laika\Employee();
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->address = $request->address;
        $employee->comment = $request->comment;
        $employee->active = $request->active == 'on' ? 1 : 0;

        $employee->save();

        return redirect('configuration');
    }

    private function validateEmployee(Request $request) {
        
        $this->validate($request,
                        [
                            'name' => 'required',
                            'email' => 'email',
                            
        ]);     
    }

}
