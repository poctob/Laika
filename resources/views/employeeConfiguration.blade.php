@include('common.errors')                        
<button type="button" 
        class="btn btn-info" 
        data-toggle="modal" 
        data-target="#employeeModal">
    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add
</button>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Address</th>
            <th>Comment</th>
            <th>Is Active</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($employees as $employee)
        <tr>
            <td>{{ $employee->id }}</td>
            <td>{{ $employee->name }}</td>
            <td>{{ $employee->phone }}</td>
            <td>{{ $employee->email }}</td>
            <td>{{ $employee->comment }}</td>
            <td>{{ $employee->address }}</td>
            <td>{{ $employee->active }}</td>
            <td>Edit Delete</td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="modal fade" 
     id="employeeModal" 
     tabindex="-1" 
     role="dialog" 
     aria-labelledby="employeeModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="employeeModalLabel">Modal title</h4>
            </div>                                                                                                           
            <form action="employee/save" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">                                        
                    <div class="input-group input-group-sm">
                        <div class="form-group">
                            <label for="employeeName">Name</label>
                            <input type="text" name="name" class="form-control" id="employeeName" placeholder="Joe Blow">
                        </div>

                        <div class="form-group">
                            <label for="employeePhone">Phone</label>
                            <input type="tel" name="phone" class="form-control" id="employeePhone" placeholder="111-111-1111">
                        </div>

                        <div class="form-group">
                            <label for="inputEmail">Email address</label>
                            <input type="email" name="email" class="form-control" id="inputEmail" placeholder="name@example.com">
                        </div>

                        <div class="form-group">
                            <label for="employeeAddress">Address</label>
                            <textarea name="address" class="form-control" rows="3" id="employeeAddress"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="employeeComment">Comment</label>
                            <textarea name="comment" class="form-control" rows="3" id="employeeComment"></textarea>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="active" type="checkbox" id="employeeActive"> Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="employeeModal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>                    
