<html>
    <head>
        <title>Configuration Data</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <script src="js/jquery-2.1.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>        
    </head>
    <body>
        <div class="col-md-10">
            <div class="page-header">
                <h2>System Configuration</h2>		
            </div>

            <div class="tabbable">
                <ul class="nav nav-pills" role="tablist">                   
                    <li class="active"><a href="#employees" role="tab" data-toggle="tab">Employees</a></li>
                    <li><a href="#positions" role="tab" data-toggle="tab">Positions</a></li>
                    <li><a href="#privileges" role="tab" data-toggle="tab">Privileges</a></li>
                    <li><a href="#time-off" role="tab" data-toggle="tab">Time Off Status</a></li>
                    <li><a href="#advanced" role="tab" data-toggle="tab">Advanced</a></li>
                </ul>

                <div class="tab-content">                    
                    <div id="employees" class="tab-pane active">
                       @include('employeeConfiguration', ['employees' => $employees])
                    </div>
                    <div id="positions" class="tab-pane">Positions</div>
                    <div id="privileges" class="tab-pane">Privileges</div>
                    <div id="time-off" class="tab-pane">Time off</div>
                    <div id="advanced" class="tab-pane">Advanced</div>
                </div>

            </div>
        </div>        
    </body>
</html>
